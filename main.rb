require_relative 'cookbook_manager'

if ARGV.count != 1
  puts "Usage: #{File.basename($0)} cookbook_name"
  exit 1
end

@cookbook = ARGV[0]
@sandbox_path = "#{File.dirname(__FILE__)}/cookbooks_#{@cookbook}"
@cookbook_mgr = CookbookManager.new(@sandbox_path, @cookbook)
@cookbook_mgr.download_cookbook_with_dependencies