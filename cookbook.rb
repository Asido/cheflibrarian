class Cookbook
  private
  METADATA_FILE = 'metadata.rb'

  public

  attr_accessor :metadata

  def self.create_with_directory(cookbook_directory)
    raise 'Cookbook directory must be specified' if cookbook_directory.nil? || cookbook_directory.empty?

    metadata_path = File.join(cookbook_directory, METADATA_FILE)
    raise "Invalid cookbook: no metadata file #{METADATA_FILE}" unless File.exists?(metadata_path)

    instance = Cookbook.new
    instance.metadata = File.read(metadata_path)
    raise "Invalid cookbook: metadata is empty" if instance.metadata.nil? || instance.metadata.empty?
    instance
  end

  def self.create_with_metadata(metadata)
    raise 'Metadata cannot be empty' if metadata.nil? || metadata.empty?

    instance = Cookbook.new
    instance.metadata = metadata
    instance
  end

  def find_dependencies
    dependencies = []

    @metadata.gsub!(/\r\n?/, "\n")
    @metadata.each_line do |line|
      line.gsub!(/\s/, '')
      if line =~ /^depends.*/
        line.scan(/"([^"]*)"/) { |match| dependencies << $1 }
      end
    end

    dependencies
  end
end