require 'test/unit'
require_relative '../cookbook_manager'

class CookbookManagerTest < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_initialize
    assert_raise(RuntimeError) { CookbookManager.new(nil, nil) }
    assert_raise(RuntimeError) { CookbookManager.new("", nil) }
    assert_raise(RuntimeError) { CookbookManager.new(nil, "") }
    assert_raise(RuntimeError) { CookbookManager.new("", "") }
    assert_nothing_raised(RuntimeError) { CookbookManager.new("#{File.basename(__FILE__)}/SomeCookbook", "SomeCookbook") }
  end
end