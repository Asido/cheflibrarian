require 'test/unit'
require_relative '../file'

class FileTestBla < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  # Fake test
  def test_extract_tar_gz
    File.extract_tar_gz('apt.tar.gz', './')
    assert(File.directory?('apt.tar.gz'))
    File.remove_directory('apt')
  end
end