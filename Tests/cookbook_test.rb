require 'test/unit'
require_relative '../cookbook'

class CookbookTest < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  # Fake test
  def test_find_dependencies
    metadata =
        "name             \"mono\"\n" <<
        "maintainer       \"Youscribe\"\n" <<
        "maintainer_email \"guilhem.lettron@youscribe.com\"\n" <<
        "license          \"Apache 2.0\"\n" <<
        "description      \"Installs/Configures mono\"\n" <<
        "long_description IO.read(File.join(File.dirname(__FILE__), \"README.md\"))\n" <<
        "version          \"0.0.4\"\n" <<
        "\n" <<
        "depends          \"git\"\n" <<
        "depends          \"apt\"\n"

    cookbook = Cookbook.create_with_metadata(metadata)
    dependencies = cookbook.find_dependencies

    assert_not_nil(dependencies, 'Returned dependencies is nil')
    assert_operator(dependencies.length, :>=, 2, 'Suspicious dependency count')
    assert_includes(dependencies, 'git')
    assert_includes(dependencies, 'apt')
    dependencies.each do |dep|
      assert_match(/^[\w_]+$/, dep, "Invalid dependency name: '#{dep}'")
    end
  end
end