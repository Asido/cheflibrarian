require 'fileutils'
require 'rubygems/package'
require 'zlib'

class File
  def self.create_directory_if_not_exist(directory)
    unless File.directory?(directory)
      FileUtils.mkdir_p(directory)
    end
  end

  def self.remove_directory(directory)
    if File.directory?(directory)
      FileUtils.rm_rf(directory)
    end
  end

  def self.extract_tar_gz(archive_path, extract_dir)
    Gem::Package::TarReader.new(Zlib::GzipReader.open(archive_path)) do |tar|
      destination = nil

      tar.each do |entry|
        if entry.full_name == '././@LongLink'
          destination = File.join(extract_dir, entry.read.strip)
          next
        end
        if destination.nil?
          destination = File.join(extract_dir, entry.full_name)
        else
          destination = File.join(destination, entry.full_name)
        end

        if entry.directory?
          File.delete destination if File.file?(destination)
          FileUtils.mkdir_p(destination, :mode => entry.header.mode, :verbose => false)
        elsif entry.file?
          FileUtils.rm_rf destination if File.directory?(destination)
          FileUtils.mkdir_p(File.dirname(destination)) unless File.directory?(File.dirname(destination))
          File.open destination, "wb" do |f|
            f.print entry.read
          end
          FileUtils.chmod(entry.header.mode, destination, :verbose => false)
        elsif entry.header.typeflag == '2' #Symlink!
          File.symlink(entry.header.linkname, destination)
        end

        destination = nil
      end
    end
  end
end