require_relative 'file'
require_relative 'cookbook'
require 'open-uri'

class CookbookManager
  private
  attr_accessor :sandbox_path, :cookbook_name

  def download_cookbook(directory, cookbook_name)
    raise 'directory must be specified' if directory.nil? || directory.empty?
    raise 'cookbook_name must be specified' if cookbook_name.nil? || cookbook_name.empty?

    File.create_directory_if_not_exist(directory)

    url = "https://supermarket.getchef.com/cookbooks/#{cookbook_name}/download"
    print "Downloading chef cookbook: #{url}."

    archive_path = File.join(directory, "#{cookbook_name}.tar.gz")

    begin
      open(url) do |remote_file|
        content_length = remote_file.meta['content-length'].to_i
        puts " #{content_length / 1024}KB"

        open(archive_path, 'ab') do |local_file|
          local_file.write(remote_file.read)
        end
      end
    rescue Exception => e
      puts "ERROR: Downloading chef cookbook '#{cookbook_name}' has failed: #{e.message}"
      return
    end

    File.extract_tar_gz(archive_path, directory)
  end

  def process_cookbook_and_dependencies(directory, cookbook_name)
    raise 'directory must be specified' if directory.nil? || directory.empty?
    raise 'cookbook_name must be specified' if cookbook_name.nil? || cookbook_name.empty?

    cookbook_path = File.join(directory, cookbook_name)

    unless File.directory?(cookbook_path)
      download_cookbook(directory, cookbook_name)
    end

    cookbook = Cookbook.create_with_directory(cookbook_path)
    cookbook.find_dependencies.each do |dependency|
      process_cookbook_and_dependencies(directory, dependency)
    end
  end

  public

  def initialize(sandbox_path, cookbook_name)
    raise 'sandbox_path must be specified' if sandbox_path.nil? || sandbox_path.empty?
    raise 'cookbook_name must be specified' if cookbook_name.nil? || cookbook_name.empty?

    puts "Initializing cookbook manager for cookbook '#{cookbook_name}' in path #{sandbox_path}"
    @sandbox_path = sandbox_path
    @cookbook_name = cookbook_name

    File.remove_directory(@sandbox_path)
  end

  def download_cookbook_with_dependencies
    process_cookbook_and_dependencies(@sandbox_path, @cookbook_name)
  end
end